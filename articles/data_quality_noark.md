# DQ Article

_Title : The migration and preservation of six Norwegian municipality record 
keeping systems - lessons learned_

## Abstract 
This paper discusses the lessons learned after undertaking a preservation 
migration of six Norwegian municipality record keeping databases. A number 
of issues were discovered that were a hinder to the migration process 
including deviation from the governing standard, file-format heterogeneity, 
collection volume, time and database structure evolution. The findings are 
presented with a discussion on how the issues become a problem for 
preservation and make an argument that a records manager should have 
greater focus on understanding the underlying database and the implications 
the state of the database has when undertaking a migration for preservation 
purposes.   

## 1. Introduction
Migration is a task that many information managers know they will face one 
day and it is one that always carries a certain level of suspense associated 
with it, as one can never truly know whether a migration process has been 
undertaken successfully or not. To the uninformed, migration may appear to
be a simple process of transforming data from one database structure to 
another or transforming sets of documents from one file format to another, 
but in reality it is a lot more complex and difficult. ISO-15489 \[REF + page
 num\] defines a migration as  the "act of  moving records from one system to 
another, while maintaining the records' authenticity, integrity, reliability 
and usability". This definition implies migration is a much more difficult
task than simply undertaking a transformation.

In this paper, the lessons learned when undertaking a migration 
project for six Norwegian municipalities are detailed, a journey many 
Information Systems (IS) professionals have taken and one that many will take.
The project was undertaken between 2011 and 2014 and consumed in excess of 2000 
man-hours of effort. The lessons learned add insight to a migration 
process in general and address issues to be aware of when migrating databases
for preservation. Further, these experiences may help an organisation 
establish a best practice when undertaking a migration project and avoid some
of the pitfalls that were faced in this project. 

In this project, a migration of both data within a number of databases
as well as a significant document collection (consisting of over 850 000 
documents), with an additional factor that the migration was solely for 
preservation purposes.  

Norway is in a very unique situation as a standard exists that concerns both 
record keeping and preservation. The standard is called Noark and  translates
to _Norwegian Archive standard_.  Its use is enshrined  as a regulation 
in law \[REF LOVDATA https://lovdata.no/forskrift/2017-12-19-2286/§3-1\]. 
This means that government agencies are required by law to use it to capture 
and preserve records, where particular focus is put on correspondence. 

The background for the migration project stems from an invitation the 
Department of Archivistics, Library and Information Science received asking 
if we would look at the preservation situation for a set of municipalities 
that were using a shared Noark 4 system. The invitation was interesting as it
offered the opportunity to undertake a data quality analysis of the 
databases and this would allow the department to build expertise within the 
area of record keeping and preservation. In addition the migration project 
had the potential to provide insights with regards to how the Noark 4 
standard functions in practice. 

The remaining paper is structured as follows: Section 2 describes the record 
keeping tradition in Norway with particular attention on the Noark standard. 
Related research is presented in Section 3. Section 4 describes the data 
collection and the procedure employed in the migration project. The findings 
are detailed in Section 5. Section 6 discusses the lessons learned  from the 
findings. Section 7 wraps up with a conclusion, reflections about the 
migration project and outlines future work.
 
## 2. Record keeping traditions in Norway
The concept of electronic record keeping and preservation has been central to
the Norwegian way of thinking about record creation as the Noark standard can
trace it roots back to 1984 and has undergone many iterations. A Noark record
keeping system \[Hagen Sataslåtten, O] is both a 
correspondence archive of the public administration, and also a 
documentation archive of the public administration in the function as 
executive authority. At a high level view a Noark record keeping system 
consists of a database and document collection. With Noark 4, 
it was a requirement to use a relational database where records describing 
correspondence between citizens and public administration are stored. The 
exchange of information between citizens and public administration is stored 
in a document collection. Examples of such exchanges are letters, notes, 
bills, applications etc. In this regard Noark is very much a record keeping 
system documenting the daily administrative activities. Noark also serves a 
secondary function as an enabler for preservation, allowing for the 
extraction of both the records and the document collection.

Noark also serves as an important source of records for freedom of 
information (FOI) requests \[Hagen Sataslåtten, O]. Having a formalised record 
keeping of government records as part of a standard, it also enabled easier 
access for citizens when undertaking formal FOI-requests. 

From a more general information science perspective, one could say that a Noark 
record keeping system is a digital library with particular requirements to 
integrity and authenticity, as well as having functionality to extract 
records to a formalised structure to aid cost effective long term 
preservation. In essence Noark creates a bond between the record keeping phase 
and preservation phase of government records. To a certain extent the Noark 
standard can be seen as an implementation of the elusive Australian records 
continuum model \[McKemmish2001\] where records can be seen both as current 
and archival records at the point of creation.

Noark 4 came in 1999 and was the first fully electronic record keeping / 
preservation standard in Norway \[Hagen Sataslåtten, O]. Earlier versions of
the standard were constrained to electronic metadata descriptions
of paper-based material.  

The Noark standard was further developed and in 2008, Noark 5 was published 
and one of the stated goals of the new version was more flexibility with 
regards to interpretation of records. However, there were numerous Noark 4 
systems in public administration at various levels when the migration project
was undertaken.  

When undertaking a migration, the data in the database tables is migrated to 
a set of XML-documents. The structure of these XML-documents are defined by 
the Norwegian National Archive.

The migration project was based on a system developed to be in compliance 
with version 4 of the Noark standard.  The Noark 4 standard consists of a 
detailed domain model and a recommended implementation of the domain model in
a relational database. The standard details a list of 95 tables that should 
be in place for a full system. The standard covers a number of various 
functionality areas and as these areas may not be relevant to all public 
administration offices various, there will be varying implementations of 
the systems resulting database structures. In general, a standard should not 
define a database structure, but leave the implementation of the underlying 
domain model to the vendor. However, when it comes to archiving, a certain 
number of inter-data connections must be in place to provide adequate 
descriptions of the material.

This forces a Noark 4 system to adhere, as far as possible, to a given 
domain model and provides the opportunity to explore the consequence of any 
deviations.

## 3. Related research

To the best of our knowledge, this is the first academic work that details 
and provides insight into an actual migration process of government records 
for preservation purposes. As such, there is little directly comparable 
related research. Rather, the context the work is placed within is a broader 
approach to migration and preservation in general. 

There are many articles that discuss the preservation process, both from a 
general perspective but also reflections of practice. More general 
literature include the Open Archive Information System (OAIS model) 
\[CCSDS, 2012\] and \[ISO 14721:2012\], which is one of the decisive guiding 
standards for preservation. The information model presented in OAIS defines a 
set of properties that a migrated database should encompass, including 
content information descriptions and preservation descriptive information. A 
Noark 4 migration object meets only some of the requirements of an OAIS 
information package due to historical reasons.     

\[Lim et al 2003\] discuss general issues related to volume and authenticity. 
The findings in this paper reflect some of their findings, but [\[Lim et
al 2003\] have an institutional perspective on how to preserve objects that 
have been migrated, while this project is about the previous stage, how to 
migrate a database. \[Lim et al 2003\] is also at a higher overview level, 
while this project concerns itself with a technical and practical approach.  

One the challenges here was how to undertake the mapping where there was a 
partially correct database structure that had to be mapped to a correct 
structure defined by the Noark standard. A lot of the literature in relation 
to mapping and database structures are a level that provide little insight to
the migration project. \[Maatuk et al\] for example discuss the problem of 
mapping between database structures from high-level point of view 
but provide little insight for our case. High level approaches to mapping 
databases to XML or other database models do not provide that much insight as
the approaches are more generic in nature, while the problem at hand is about
two particular database structures. Also, the mapping problem was reduced to 
direct or indirect mapping between tables, with a need to insert or update 
data and in some cases aggregate the contents of tables.

There exists an ISO standard, ISO 13008:2012 that covers conversion and 
migration. This standard gives more high-level advice to approaching the 
problem of migration. In hinsight, the migration project should 
have undergone better planning, but in the context of ISO 13008:2012, it would
not had any significant impact, as a lot of the experienced problems were at 
a technical and practical level.  

As a record keeping and preservation standard, Noark has achieved limited 
success. Two reports of note provide an insight into this \[REF 
RRR\]\[MENONRAP\]. The first is a General Auditor report \[RRR2010] from 2010
detailing the state of preservation of electronic records for public 
administration at the municipality level in Norway. The report indirectly 
shows Noark in a positive light in that it notes that record keeping that was 
not addressed by an preservation standard was seen to be more problematic 
than record keeping that was addressed by a preservation standard. The second
report \[MENON] is a recent report that evaluates the Noark standard and 
details a number of problematic issues. In many ways the issues described in 
the report relate more to market concerns rather than focusing on problems 
with the standard itself. The results of migration project add value to these
reports as it highlights directly the problems that arise directly related to
implementation issues.    

### 4. Data collection

The migration project is based on the record keeping databases and document 
collections from six different Norwegian municipalities. The average 
population count of the municipalities was approximately 3700, while the 
population count of the largest municipality was just under 5000. The record 
keeping system that sat on top of the databases was relatively old, dating 
back to mid-nineties. As such the system and database have undergone multiple 
development iterations. The system in question was in production for
the six municipalities from 1999 to 2012, resulting in a thirteen year span 
of record creation. 

There were 157 211 case files across the six databases, with the the largest 
municipality have accrued 42 000 case files spread across the 13 years, while
the smallest municipality had 14 500 case files. Each case file averaged 
roughly 6 registered documents per case file. The largest case file had in 
excess of 200 documents registered. This was a case file detailing a job 
application process for a particularly attractive position in the municipality. 

The largest document collection was over 22GB in size. The largest file in 
the collection was a 235MB PDF document. Table 1. shows a count of the 
various documents in the databases and the original file format the document 
adheres to along with their preservation equivalence. There was in excess of
850 000 documents in the document collections. It is worth noting that many 
of the PDF documents are preservation versions of the production-format (doc,
 docx etc) documents, so a high percentage of the 850 000 are actual duplicates.

| Original file format | Preservation file format | count |
|-------|-----------|---------|
| BMP   | JPEG      | 100
| CSS   | N/A       | 1
| DOC   | PDF/A     | 327000
| DOCM  | PDF/A     | 12
| DOCX  | PDF/A     | 1360
| DWF   | PDF/A     | 1 
| DWG   | PDF/A     | 29 
| DXF   | PDF/A     | 3
| EXE   | N/A       | 5 
| GIF   | JPEG      | 1227
| PDF   | PDF/A     | 342000 
| HTML  | PDF/A     | 19000 
| JPEG  | JPEG      | 4100
| LWP   | PDF/A     | 1431
| MOV   | MPEG2     | 1     
| ODT   | PDF/A     | 25
| PPT   | PDF/A     | 280
| PPTS  | PDF/A     | 1
| PPTX  | PDF/A     | 30
| PNG   | JPEG      | 600
| RTF   | PDF/A     | 395
| TIF   | TIFF      | 493
| TIFF  | TIFF      | 2
| TXT   | RA-TXT    | 42461
| XLS   | PDF/A     | 958
| XLSM  | PDF/A     | 1
| XLSX  | PDF/A     | 110
| XPS   | PDF/A     | 1
| XML   | RA-XML    | 4 410
| ZIP   | N/A       | 64

Table 1. A list of the count instances of various file formats across the 5 
databases and their preservation equivalent. In some instances number counts 
were rounded.

The interesting information in Table 1 is the amount of heterogeneity in the 
document collections. There are 30 different registered file types, a fact 
that reflects how the system has been used and what users can expect to find 
there in the future. 

The municipalities had accumulated thirteen years (1999-2012) of records that 
were stored in their databases. It was noted that the software vendor claimed 
they were capable of extracting the data with their own extraction tool, but the 
associated costs of using it was so high that for all intents and purposes the 
tool did not exist. This is likely to be a result of the municipality being 
an early adopter of the system and there was a number of problems within the 
database that were causing problems when undertaking a migration. The 
municipalities were likely to also be early adopters for the extraction tool 
and would have to carry a heavy burden of the development costs.

A general recommendation is that records should periodically be deposited 
with a municipal archive institution to ensure the municipality is in 
compliance with its archival obligations. Given the municipalities had  
accumulated 13 years of records, it became a pressing issue to make 
sure the material is available for future generations. 

Each individual database consisted of over 100 various tables. Within a 
database it was possible able to identify 70 tables that were relevant to the 
Noark 4 standard. The problem here was that, while the system claimed 
compliance with the Noark 4 standard, it did not employ a database structure 
easily  identifiable with the standard and would require extensive mapping of
tables and columns to transform the database to the correct structure.The 
Noark 4  standard consists of a set of 95 tables. Some of the missing tables 
were Noark 4 functionality that was not supported by the system. The system also 
contained additional information that was not covered by the Noark standard. 

To solve the problem the project also developed a free and open source tool 
(Footnote: Link to standard after acceptance) that creates a Noark 4 database
structure in accordance with the standard. The tool then copies records from
the original database and fixes relevant problems. Finally the tool creates the 
set of XML-documents fulfilling the migration process.

#### 4.1 Description of procedure

The migration project was approached on an ad-hoc basis, a cardinal mistake 
when undertaking a migration. It is easy to be critical in hindsight that no 
detailed planning procedure was undertaken, where the work was better 
structured, but the migration project had no clear starting point. There were
so many different tasks to be undertaken. The first step was an analysis of the 
database and Noark 4 standard and focused solely on solving the problem at 
hand from a technical perspective. Our goal was to decipher the database in 
order to migrate the data for preservation purposes. The approach was very much 
investigative and ad-hoc in nature and a complete understanding of the project 
did not emerge until about a year into the project. There was neither system 
nor technical documentation available, but access to user manuals and the 
running system was available. A further limitation was that as the Noark 
record keeping system was to be replaced by another system, there was 
limited time left where it was possible to verify how the system inserted and
updated records in the database! The actual distance (in kilometres) 
rendered it infeasible to access the system leaving the project working out 
of a copy of the data in the databases. The structure of the databases were
the same across all the municipality databases, so efforts were concentrated 
on the largest database, which corresponded with the largest municipality. 

Working off a copy of the databases most of the time in the project was 
dedicated to analysing the database structure of the original database 
structure, while at the same time developing an understanding of the Noark 4 
standard. A correct Noark 4 database was implemented and step by step records
were identified in the original, copied over to a correct structure, fixing 
any problems that arose. After this work was finalised, effort was spent on 
migrating the data to XML-documents. Once records were copied over, 
the attention shifted to working with the documents and ensuring all 
documents were possible to be associated the records in the database in saved
in a file format suitable for long term preservation. 

## 5. Findings

A summary of the main findings show that the following issues were the main 
hinders in our migration project:

  - Automated Document Migration
  - Database evolution
  - Database issues
  - Deviation from the standard
  - Wrong system use
  - Missing data
  - Additional data
  
### 5.1 Automated Document Migration

Document migration is an issue most record keeping systems will 
have to deal with at some stage. This is particularly true when documents 
adhere to older file formats e.g Lotus Word Pro or Wordperfect. When 
migrating the contents of a record keeping system for preservation purposes 
it is important to ensure the employed file formats the documents are 
migrated to are suitable for long term preservation. File format used for 
documents in their daily use (e.g docx for text documents) are referred to as 
_production_ versions and corresponding long term preservation 
version (e.g PDF/A for text documents) as _preservation_ versions. PDF/A is 
good example of preservation file format as the document and its contents are
self-contained. Self-containment ensures requirements for future rendering (e.g 
fonts) are contained within the document, while functionality that can 
hinder preservation access e.g preventing printing, password protection are 
prohibited. 

In general a document migration process is risky, as it is often automated 
and the outcome of the process can be difficult to verify. The migration 
project undertook an analysis of all file formats for the various documents 
stored within the record keeping system, the results of which are detailed in 
previous section in Table 1. At a glance such a list may not seem daunting at
all, but each additional file type comes with a knowledge requirement that can
be expensive to define and understand. The competencies required to handle 
the various file formats is extensive, the available software to undertake 
the conversion is limited and with batch processing the result is often 
unverifiable. File format heterogeneity combined with volume, cost, time and 
technological obsolescence impede automated migration.  Volume compounds the 
issue as it increases the time required to undertake the actual conversions,
but there are also other minor issues that make processing difficult. Cost 
is directly related to volume and file format heterogeneity. File formats 
that have no obvious preservation equivalent e.g. DXF and DWF (a CAD format  
for storing two- and three- dimensional design data and metadata) and 
ZIP files (contents have to be  extracted and checked) quickly become a hinder
to automation and batch processing. Their existence in the system can add weeks 
or months of work onto a automated migration/preservation process due to various
inquiry and investigations to be undertaken and ultimately cause long delays 
and significantly increased costs. 

Ironically, small numbers of non-standard file types can be quite difficult 
and expensive to deal with and
their migration cost can be as high as large numbers of more widely adopted
file formats. An example of this is dealing with the DWG  files as 
opposed to the doc files. The doc files were batch converted using a PixEdit
(migration software) installation, while the DWG were migrated using open 
source tools or manually converted in appropriate software. In hindsight, 
dealing with the large number of doc files was much easier as the only 
requirement was a queuing mechanism to the PixEdit instance. 

Volume and time are related as the volume naturally increases over time. When
the system went into production around 1999, Lotus Word Pro was the dominant 
word processor in use. At various points in time the municipalities changed to 
Microsoft Office and the word format became the dominant file format stored 
in the system. As time went by the volume increased, but slowly the software 
to handle LWP-files fell away. Technological obsolescence quickly becomes a 
factor that causes unforeseen difficulties in the future. These issues then 
become a hinder when attempting automated migration process.

An example of some of the issues faced when undertaking such a migration. 
During the project neither MS Office, OpenOffice nor LibreOffice software were 
successfully able to convert LWP files. OpenOffice was able to open some LWP
files but the software often crashed when opening files claiming compliance 
the the LWP file format. An example of the difficulty in dealing with these 
type of files is how in once case an attempt to convert a LWP document to PDF
resulted in a 93-page PDF/A full of binary symbols. OpenOffice did not report
any issues or errors when attempting the conversion.

Interestingly a discussion with the archive community resulted in a 
suggestion where someone had a copy of an old magazine with a copy of 
software that they believed could successfully open the LWP file format. From 
there it would was possible to copy and paste the document contents into 
another program and export as PDF/A. This strategy was not pursued due to 
time constraints. when studying the LWP file format it was noted that the 
format appears to be a combination of plain text and binary symbols so a lot of
the written content of the document is retrievable using manual labour with a
text editor. Some LWP documents were not converted to PDF and it was left to 
future users to extract the information if it was required.  

A recommendation from this work is that an IS-professional in charge of record
keeping or other digital libraries should have a ongoing strategy that 
identifies heterogeneity and volume and should be able to identify hinders for
migration. It is the slow changes over time that cause most of the problems. 

### 5.2 Database evolution

The record keeping system that was subject to investigation has been in 
existence from at least the early to mid nineties. The software is in itself
a mature product and has undergone multiple versions and changes. the 
migration project in essence only had a copy of the database belonging to 
the latest version of the system to work with. There was no access to 
technical documentation, nor was there made an attempt to request access to 
documentation as more often than not such documentation is deemed 
confidential and seen as internal documentation. The database itself noted 
that it was in compliance with a particular version of the record keeping 
system and there were traces of logs showing changes to the database. However
this information was not really that useful as it was in a format that was 
too complex to easily understand.  

Database evolution can be a challenge to migration as relationships between 
tables and columns can change. Data can be stored in different places during 
various time periods and such changes make it difficult to automate the 
migration process, especially if documentation is unavailable. 

In the databases that was investigated, multiple changes in database 
structure during noted. A major upgrade to the record keeping system in 2006 
saw the introduction of multiple new tables. This was visible as data in these 
tables had an earliest date recorded as being in 2006. It was also discovered 
that records in other tables could take on a new form after 2006 compared to how 
they were stored earlier.

An example of this is in the way comments created by case handlers 
and record keepers that were associated with records should have been stored
in a particular table called 'comments', but were in fact stored in multiple 
different tables. An assumption was made that the system previously 
interpreted the field as being in a one-to-one relationship between 
applicable entities, while it was in fact a one-to-many relationship and at 
some point the database evolved and this issue was corrected resulting in a 
unfortunate situation where it became unclear where the actual data was to be
found. 
 
The morphing of the database structure is a witness testimony not only of the 
technology changes between 1999 to 2012, but also how record keeping changed 
during that time period. The use of associated software with the record keeping
system showed changes from Lotus Word Pro to Microsoft Word. Earlier 
spreadsheet documents were rare but became a more common occurrence later. The 
record keeping process went from being a combination of paper and digital 
record keeping to becoming more and more digital, especially as the use of 
email became more widespread. 

Prior to 2006 the system only stored outgoing documents electronically. 
Incoming documents were registered in the system but remained  paper-based. 
After 2006 incoming letters were scanned and stored within the system. At 
some point many incoming letters on A4 paper were replaced by emails.  For 
example Lotus Word Pro (LWP) was in use around 1999 but was superseded by
the word format (doc) around 2004. Eventually the MSOOXML-format (docx) took 
over for the word format.   

As with any system, it is expected that minor updates will see minor changes to
the underlying database while major changes in functionality will see larger 
changes to the database so it is natural that the database structure _morphs_ over 
time.  Today tools like [liquibase](REF) exist that nicely document database
evolution. This type of useful documentation will be lacking in legacy 
systems. 

A recommendation here is that an IS-professional should ensure they are in 
possession of updated documentation regarding the evolution of the database. 
This should be done in order to provide insight into the gradual changes in 
technology that ultimately become a challenge for migration and preservation.
Even a relatively short time period of 13 years can see relatively large
changes to the database. 

Database management can be a difficult issue to tackle however, as a database
can quickly grow to have many hundreds of database tables. Also a large 
sized municipality in Norway can quickly have over 100 record keeping 
systems with their own databases. The municipality of Oslo for example 
reports 666 \[RRR] various record keeping systems and databases containing 
documentation with a preservation order. It can be both costly and difficult 
to see the  long-term benefit of understanding the role of each table in each
database and how it changes over time in order to be able to support a 
potential preservation migration project in the future. But this is actually the 
adopted approach by a number of municipalities in Norway where work is being  
undertaken to analyse and understand the significant tables in some of the 
more important record keeping systems.

Many IS-professionals will gladly argue that information should be treated 
like an asset for the organisation, but can quickly fail to understand the 
need to understand the database at Entity-Relationship model level. It is 
very easy to fall into a practice where the technical side of things remains 
a vendor issue. In the migration project it is clear that this problem is 
particularly prevalent in situations when society moves from paper-based record 
keeping to electronic record keeping. Database evolution likely has a greater
impact on database evolution when moving from paper to electronic record 
keeping.

It is worth noting that the push to cloud is likely to have an impact in the 
future when dealing with migration but it is unclear what the impact will be.
Self-hosting software meant the organisation managed and had full access to 
their databases. Some cloud-based Software as a Service (SaaS) style solutions 
for record keeping can store the data of multiple municipalities in a single 
database with potential problems when the need to migrate the data occurs. 

However for self-hosted solutions, if the software vendor follows a best 
practice, technical documentation should be readily available and up to date.
For older systems, the vendor may have ceased trading, been bought up or for
some reason no longer supports the software. In the worst case documentation 
may no longer be available and the process of migration can become extremely 
complicated.

### 5.3 Database issues
When analysing the databases, it was discovered that none of the databases had 
referential integrity enabled, nor were primary keys defined for the 
individual tables. Referential integrity is an important database mechanism 
that can be used to ensure consistency of data within a database. An example 
of this is that  referential integrity can prevent a user from registering a 
document within a database unless there exists a connection to a case-file 
and a case-handler. Referential integrity in can and should be used to 
prevent the occurrence of 'orphan'-records.  

The non-use of referential integrity caused two issues. The first is that the
Noark standard required referential integrity in the conceptual model to 
ensure that records could not be inserted into the database without 
the correct context (parent-child relationships). The second is related to the
use of pre-defined values e.g. status values.  

Both issues are problematic when it comes to testing the compliance of the 
extracted data as the test software (testing the output of a migration) will 
expect that the data in the database is in compliance with referential 
integrity. As such, any deviation from the standard will result in errors. For 
important records (case files, document identifier) the system did manage to 
successfully enforce a level of referential integrity through software,  
rather than at the database level. There were a number of examples where 
referential integrity issues caused problems, but for the most, this seemed to
be related to third part integrations rather than problems with the vendors 
software. The municipalities used a number of third-party systems that were 
integrated to the Noark record keeping system and these third-party 
integrations accessed the database directly inserting data without 
necessarily knowing that the data insertion could have a consequence on later
migrations. In some cases, they created orphan records. There was also an 
instance where a small collection of records had been disconnected from the 
official collection of records because a user had changed a field connecting
the records to the correct context. A user had changed the parent identifier,
resulting in a invalid reference for the children records linking to a 
parent record. Had referential integrity been enabled in this case, then this
would not have been possible. The reference in the children would have 
automatically been updated. 

The second issue is related to the use of status values e.g. a case closed 
status value. If referential integrity was switched on and relevant columns 
were defined as foreign keys then the required status values could be 
enforced at the database level, reducing a number of potential problems when 
undertaking a migration. Such issues were present but manifested them mainly 
as duplicates due to a lack of primary keys.

Primary keys play an important roll preventing duplication of records 
resulting in inconsistent data. As primary keys were not defined there were 
instances of status values being defined multiple times, but also in some 
cases they had multiple meanings. 

\[Duranti\] notes that the lack of unique identifiers can raise issues 
regarding to the perceived authenticity of records. Given that referential 
integrity and the use of primary keys was not required, it could be expected 
that questions regarding the authenticity of the records with these databases
could be raised. Within the context of Noark there are two particular records
that need unique identifiers. The first is a case-file and this is normally a
string consisting of the year followed by a sequential case number 
incrementing for each new case-file. An example of this is _'2019-00001_' or 
_'2019-00100'_ depicting the first and one hundredth case-file during the 
year 2019. The second is the registration of a main document and any 
additional document attachments within a case-file. Each document and 
attachment are uniquely identifiable within the case-file. 

Despite the lack of primary keys within the database, the software did a 
surprisingly good job at ensuring uniqueness of both case-files and document 
registrations. These are the most important objects when considering a 
preservation migration. As such there is no real criticism to lay forth that the 
system was unable to ensure the authenticity of the records. The lack of 
referential integrity and use of primary keys became more of a cost
issue during the migration project as opposed to being an authenticity 
issue. Considerable effort was used to detect and remove duplicates as 
well as ensuring consistency between records. 

It is likely that the non-use of referential integrity is likely to be related
to legacy issues. The  system in question came into existence in the 1990s 
and was likely to have been an implementation of a version of the Noark 3 
standard. There may have been a need for flexibility in the database 
structure where the job of enforcing referential integrity is left to the 
software rather then being a fail-safe data integrity mechanism implemented 
in the database. Over time the system may then have had to live with legacy 
data and subsequently it was not deemed possible to enable referential 
integrity.  

A recommendation for an IS-professional within this sphere is that it is 
important to understand the conceptual model of the database and how it is 
implemented. In some cases one simply cannot enforce referential integrity 
throughout the database, especially if the conceptual model requires a 
large degree of flexibility in how records are interconnected. But it is 
certainly disreputable to have neither referential integrity nor primary 
keys in use in a modern system built on top of a relational database. 
Responsibility for a system includes understanding how that system uses the 
database.

There were also a lot of minor issues in the database. A common issue was 
how the *null* value was often used to indicate a '0'. But not always. The 
Noark 4 standard often required a boolean value to be set as either a '0' for
false or a '1' for true. 

When it comes to the issue of using null for '0', one can argue perhaps that 
the issue really is a non-issue. The program can simply interpret a 
null as a '0'. The problem here is that further analysis is required to 
ensure that this is in fact true. Perhaps there does exist data in some table
where null, '0' and '1' are in use so across the 100 or so tables and 900+ 
columns a manual inspection and verification is required. 

However, *null* in database theory means that no value has been set for a 
particular field and this can be problematic when the field is mandatory. A 
particular field in the database is related to whether or not a record is 
not subject to publication as a result of a freedom-of-information request. 
Typically this is private or sensitive information that the law prohibits the
publication of. An example of this could be an application from an older 
person applying to live in a municipal care-home where information about 
their ability to take care of themselves would be part of the application.

Normally this process of preventing publication is a manual process and the 
information can only be withheld from publication, if a source of law is 
identified. When migrating the data from the database, assuming that a null 
values means the case file is a public case file without limitations, the 
software in essence is undertaking an evaluation rather than the evaluation 
being undertaken by a person based on inspection of the contents of the case 
file.

### 5.4 Deviation from standard
The system claimed to be in compliance with version 4 of the Noark standard. 
A few database tables did in fact follow the naming convention set forth in 
the standard, while others had names that bore no similarity to the standard.
Some of these strange table/schema names are illustrated in Table 2.
 
|  Noark 4  | System name|
|-----------|------------|
| JOURNPOST | DGJMJO 
| NOARKSAK  | DGSMSA    
 
 Table 2. Examples of table names that are not in compliance with the Noark 4
 standard
    
Columns often had similar names in accordance with with the standard, but in 
many cases the column names were simply different. Examples of this are shown
in Table 3.

|  Noark 4  | System name|
|-----------|------------|
| SA_TGKODE | UNTOFF
| SA_UOFF   | HJEMMEL
| SA_TITTEL | INNH1

 Table 3. Examples of column names that are not in compliance with the Noark 
 4 standard

In some cases the standard required a count field e.g. the number of 
records associated with a case file. This field was missing in the database, 
but could easily be produced as a result of a SQL-query.

There was one instance where two columns had their names swapped. This was not
really a major issue, but did require documentation. It was also noted that 
sometimes the database had differing datatypes and string field lengths than
the standard expected. There were a few instances where the Noark 4 standard 
specified a column length to be VARCHAR(10), while the database implemented 
the column as VARCHAR (15).  This was a potential problem when testing the 
output of the migration for compliance.
       
### 5.5 Wrong system use

There were two instances that where the system had been used incorrectly. The
first was in regard to the use of encrypted files, the second was in regard to 
document versions. Early in the process of migrating documents to 
their preservation equivalent, it was discovered that some of the documents 
were encrypted. This was unfortunate as there was no documentation about the 
employed encryption algorithm or the key to decipher it. Given the age of the
system it was likely that a simple symmetric-key encryption was employed. TIt
was not possible to figure out the decryption process and the municipalities 
were forced to pay the vendor to extract all the documents. The 
municipalities had not requested encryption capabilities in the system and 
were surprised that it actually was possible for some case handlers to 
encrypt documents. There were just a few hundred documents that were 
encrypted, but it became a contentious issue as users of the system should 
never had used the functionality. It was likely that some users felt the need
to encrypt documents as the contents were likely sensitive in nature. 

The other issues that was discovered was in relation to the systems ability 
to store multiple versions if a given document, in essence tracking how a 
document evolved. This became a problem as the the project was informed that 
versions were not in use in the system, but the database showed a few 
instances (< 10) where a document was stored with multiple versions. 
Communication and misunderstanding around this issue caused minor delays in 
the extraction process.

A recommendation here for a IS-professional is to ensure that full knowledge 
of the system and its capabilities are essential for undertaking a migration.
If a system has hundreds of users and the system and database evolves over 
time, it is likely that users will use the system in a manner the 
IS-professional is unaware of.  Documentation and testing of all 
functionality is important to combat this. Updated documentation and testing 
of new versions should also be prioritised. 

### 5.6 Missing data

There various examples of missing data in the database. The first was when 
required tables, according to the standard, were missing, while the second was
when actual expected data was missing. 

The Noark 4 standard has a number of additional optional modules of record 
keeping functionality that can be employed. When analysing the databases 
partial data related to an optional modules was found and it was unclear 
if there was remaining data somewhere else or if the municipalities 
actually had used these modules. In one instance four weeks of data from an 
optional module was found and upon investigation it was discovered that the 
municipalities had tested some of the optional modules but decided not to use 
them after all. A 13 years timespan can see different people managing the 
database and information  regarding earlier use of the system can easily be 
lost if a records manager leaves the position.  

When a database contains records from a software module that subsequently is 
discarded and is limited to a testing period of four weeks, it creates 
challenges for long term preservation. One issue that quickly arises is related
to authenticity. How can a future used (in 50 years) interpret the authenticity
of such data? It has documentation value in that there are 4 weeks of records
there, but seeing as it is related to testing it is likely that the data 
contains both real and fake information. Users in the future querying such a 
database could see only 4 weeks of data covering a 13 years time period and 
unless the use-issue is documented,  future users might come to the 
conclusion that the migration process was likely to have been flawed and the 
authenticity of the entire migrated database may become questionable.

In some cases the standard required the presence of some obligatory 
fields, but in some cases obligatory data was missing. This can be 
exemplified with an example where a administration table that contained 
information about users and group membership was sporadically missing 
required records. When a user was added to a group, the system should have 
recorded the identification of the administrator that added the user the 
group. This information was often missing. There was no reasonable 
explanation put forward as to why this information was missing, but it was 
believed to be a result of updates where the functionality came and went away.
Even though this value had relevance during the day-to-day use of the system,
it is unlikely to have value in the distant future, so the fact that it was 
missing was probably not really a major issue. In version 5 of Noark, this 
value is no longer part of a migration. A quick and easy solution to this 
problem is to ensure the column definition in the database is set to 'NOT 
NULL'. This could prevent insertion  problems for data that is in fact 
mandatory. 

This problem where updates to software cause 'holes' in records in 
the database is very much a legacy development issue. Today software developers 
will often use a Test-Driven Development \[Beck] approach where tests are 
written before the system code is written. Eventually the system has extensive 
test suite that covers a large proportion of functionality and subsequent 
updates to the system will be subjected to continuous testing before 
deployment. This does not magically solve the problem, but if standards 
compliance test are in place, it is less likely such problems will occur. 

A recommendation here for the IS-professional makes sure that any system being 
purchased comes with a test-suite and a minimum set of tests exist to ensure 
compliance with the given standard. Another issue to be aware of is that unless 
there is a formal requirement that a system has to adhere to a given data 
model for migration or preservation, it may be difficult to ascertain the 
consequence of such problems. Regular analysis of the database to see if 
information is missing may seem like a waste of time, but one cannot assume
that software updates will not have a consequence on records in the database.  

## 5.7 Additional data 

The system also employed additional functionality in addition to general 
record keeping functionality described in the Noark 4 standard. Building 
applications along with related data were also handled by the system. This 
fact also became an issue for migration as the extraction information package
had no place to store the data and such records potentially would be lost if 
vendor created an extraction that only adhered to the Noark standard.

## 6. Discussion

When a migration project experiences problems with standards compliance the 
issue can be approached as information quality issue \[REF WANG/STRONG. 
Markus??\]. If there are many problems it will have consequences on intrinsic
data quality dimensions such as accuracy, believability and reputation but 
also contextual information quality covering issues like completeness. 
Ultimately the consequence is that the database and document collection are 
seen as being non-authentic and have reduced evidential value. 

Deviation from the standard was the single largest source of problems for the
migration project. There were knock-on effects that caused a lot of problems.
The vendor can claim compliance with the standard but it can be very 
difficult to independently verify compliance. Each small deviation may seem 
like a minor problem, but many minor problems become a major problem and 
quickly take up a disproportionate amount of time when creating software to 
extract the data. It is also worth noting, that it is not common to define 
the underlying  database implementation, datatypes and column restrictions in
the way it was done in Noark 4. One must understand that Noark 4 was a 
product of its time and it was easy to fall into the trap and overspecify 
when developing the standard. 

Although, given that a standard is in place, there should be a rigorous 
compliance testing regime in place, especially with regards to functionality 
to support migration. However it is understandable that the records manager 
fails to see the need for migration requirements when a migration is not 
likely to be an issue until twelve years into the future. This makes a case 
for a compliance testing regime should be made available by the standard 
maintainer as is the case for some standards. A recommendation here is that 
the IS-professional should approach software acquisition  from a life-cycle  
perspective where end-of-life migration issues are part of the acquisition 
process.

However one cannot really simply blame the vendor for allowing deviations in 
the data model. The Noark 4 standard itself opens up for it and explicitly says 
the vendor can deviate from the standard where required. This is explicitly 
stated on page 9.  '*Datamodellen er ment som et tilbud til dem som skal 
utvikle Noark-baserte systemer, og er ikke et krav i seg selv.*'. This 
translates to '*The data model is intended as an example for those who need to 
develop Noark-based systems, and is not a requirement in itself*'. The 
standard follows up and notes that it should be possible to produce an 
extraction, and compliance with the data model is obvious. In hindsight it is
easy to criticise such an approach, but at the time it may have been 
necessary to allow this to enable the migration of data from earlier 
versions Noark (1 through 3) as well as other variations of record keeping 
standards. 

The second important factor in understanding the migration issues was how 
volume, heterogeneity and time caused problems when dealing with the 
documents when undertaking an extraction. Volume quickly becomes a problem as
it precludes manual processing, and only automated solutions are applicable 
due to costs and efficiency reasons. However the results of automated 
processes are not always verifiable and this can result in corruption and 
loss of data. Heterogeneity compounds this problem as the greater the number 
of various file formats, the greater the associated costs when undertaking a 
migration. An issue here is that an organisation might wish to capture as 
much as possible and open the record keeping system to all file formats even 
though it will cause issues for later migration. Conversely limiting the 
allowed file types will preclude documents coming under record keeping 
control, but make migration easier. The former is likely the correct 
approach, even though the final migration costs are unknown. Large 
enterprises may be in a position to mitigate this problem, 
but no universal solution exists, especially for older data. Over time 
technology changes and this results in changes to the database used byt the 
system but also how the system is used resulting in unforeseen problems with
migration.  There is no simple answer to this problem. The answer does, 
however, rest with responsibility. A vigilant IS-professional will, from the 
outset, know that this is a underlying potential problem that must be kept 
in-check and will have procedures in place to identify issues related to 
creating preservation versions of documents.    

The third impacting factor is related to database evolution. While the 
evolution itself is not really a problem, the fact that documentation of the
evolution is not readily available has an impact on preservation. A changing 
domain model where data has multiple locations during the lifetime of the 
database means data can quickly be lost. It is not clear though that an 
IS-professional has available time og skillsets necessary to solve this 
problem. The problem can be mitigated with rigorous testing, documentation 
and a requirement that it is possible to undertake a migration. But one 
should be aware of the issue. 
    
The remaining issues detailed in this paper did have a consequence but were 
in a way minor compared to three detailed above. The fact that the database 
was not in compliance with the standard and there is a requirement to process 
the database to ensure compliance results in a situation where, from an 
archival perspective, it becomes necessary to the preserve multiple versions 
of the  database. These are as follows: 

   1. Valid but potentially incorrect
   2. Invalid but Correct
   3. Original database backup
   4. Database in a neutral format 

The first is what the migration project undertook. It resulted in a migration
that was valid, but incorrect as the database was processed into something 
else in order to create an extraction.  This is definitely a result of the 
database not being in compliance with th standard. A secondary criticism that
the work received from the archival community is that future users will see 
such a migration and believe that the system was in face in compliance with
the standard. In essence the migration is counterfeit, a lie about the 
systems use. This results in the requirement of preserving the databases as 
close to the structure defined by the standard, but with lots of problems . 
Technically this would tell the truth about the system, but would also leave 
problems and missing data. As there is a potential conflict between the first
and second approach, there is a requirement to store a copy of the original 
database backup. An archive should preserves this object as long as it it 
possible to read and understand it, but its primary use is to resolve 
conflicts between the two migrated versions. Given that it is unlikely the 
original database backup will be useable in the future, the database should 
be extracted to a neutral format e.g. using Siard \[SIARD REF\]. 

Preserving four copies of the same data seems redundant and overkill but from
a preservation and authenticity perspective, it is likely that this is 
necessary. Ultimately, the problem that is solved stems from the fact 
the system deviates from the standard. It is also possible that the vendor 
with full access to system documentation is able to create a migration in 
compliance with the standard, reducing the need for multiple versions. 

## 7. Conclusions and future work

In this paper the lessons learned from a migration project based on a system
claiming compliance with the Norwegian record keeping standard (Noark 4) are
presented. Even though the project is limited to a Norwegian standard, the
results are relevant to the larger IS community where migration for
preservation purposes is to be undertaken. 

Six Norwegian municipality databases were analysed and a migration was
undertaken in order to deposit the records and associated documents with
an archival institution. The project was largely successful in
undertaking an actual migration, but was unable to provide a definitive
answer whether or not a complete migration was enacted. The migration
project achieved a coverage of about 95% and was left with a question, that to
achieve the remaining 5%, would it require as large an effort as the
initial 95%. At some point a decision had to made that enough had been 
migrated. In this case, once the most important records were migrated, it was
deemed not be worthwhile expending further resources on secondary information 
that has little future preservation value. Technically, the extraction was 
inadequate according to national guidelines, but the extraction was accepted
with the documented limitations. 

In terms of the ISO 15489 (TODO: add apage no) migration definition, that 
migration is an _"act of moving records from one system to another, while 
maintaining the records' authenticity, integrity, reliability and usability",
one can argue about the authenticity of a migration where there are so many 
problems. On one hand the migration project managed to increase authenticity 
as the original database is now extremely well documented for future users 
and there exists a copy of the records in a structure that is largely 
compliant with the standard. On the other hand, when so much work has to be 
undertaken one can be left in doubt about the quality of the records. It is 
likely that authenticity will be defined based on the amount of documentation
that was developed and the extent to which the migration is traceable back to
original database. Publishing the migration software as an open source tool 
is an important part of the final documentation. 

The most important consequence of this work is a the recommendation that a
records manager takes a greater responsibility in understanding issues
related to migration when employing a particular record keeping system. If
the system claims compliance with e.g. Moreq2012 it must be possible to
verify compliance. Bear in mind as the software evolves the database may
evolve, introducing problems. Migration will occur at some time, either
when replacing the software with another implementation or when the system is
no longer in use and the records will be subjected to a preservation
process. Ultimately, compliance testing of the system against a standard must
be a central part of any software procurement process.

It was observed during this migration project that the records managers have 
neither the time nor the technical ability to concern themselves with the 
underlying database and quality issues.  Their focus is limited to ensuring 
that the daily record keeping is correct. In Norway, the record keeping
profession is perhaps putting too much reliance on the vendors of record 
keeping systems and not taking enough responsibility. There is an often held 
view among record keepers that the archive is a software implementation, rather
than the contents of an underlying database. The relationship between system and
database is something that should be further studied.

This work also points to a requirement of further research into the
practical side of undertaking a migration. ISO 13008:2012 Migration and 
conversion is a best practice of sorts, but there is little literature that 
tackles the issue of migration from a practical and technical point of view. 
Perhaps this is because this type of migration work is more of a 
multi-disciplinary issue including metadata, preservation, mapping, 
migration, document forensics and project management. Perhaps it is because 
some see it as a solved problem where a solution is always purchasable. It is
a given, that very few public administrations will provide research access to
their record keeping databases and perhaps this project was lucky in terms of
timing and meeting the requirements of the municipalities. The approach taken
in this project was very investigative in nature, but there was no definitive
answer as to whether or not the migration project had extracted enough. It is
not advisable to naively jump into such a project, but when there are no
resources available and the migration needs to occur, the approach detailed 
here may work.
   
The results of this study are not transferable to Noark systems by other
vendors, rather they point to an issue with legacy systems that have a twenty
year plus timespan in production, that see the underlying database morph 
between various versions of the record keeping standard. The same migration 
code was informally tested on databases belonging to systems from the same 
vendor, but belonging to different municipalities. Interestingly, the results
vary.  In one case there were not as many problems, while in another case 
there were a lot more problems. The fact that there are verifiable 
differences across installations is worrying and suggests further study into 
the issue.
 
## References

\[Hagen Sataslåtten, O]
Hagen Sataslåtten, O. (2017). The Norwegian Noark Model Requirements for EDRMS in the context of open government and access to governmental information. Tidsskriftet Arkiv, 8(2). https://doi.org/10.7577/ta.2485


\[Beck, K.] 
 Test-Driven Development by Example, Addison Wesley - Vaseem, 2003


@InProceedings{10.1007/978-3-540-85654-2_58,
author="Maatuk, Abdelsalam
and Ali, Akhtar
and Rossiter, Nick",
editor="Bhowmick, Sourav S.
and K{\"u}ng, Josef
and Wagner, Roland",
title="Relational Database Migration: A Perspective",
booktitle="Database and Expert Systems Applications",
year="2008",
publisher="Springer Berlin Heidelberg",
address="Berlin, Heidelberg",
pages="676--683",
abstract="This paper presents an investigation into approaches and techniques used for database conversion. Constructing object views on top of a Relational DataBase (RDB), simple database integration and database migration are among these approaches. We present a categorisation of selected works proposed in the literature and translation techniques used for the problem of database conversion, concentrating on migrating an RDB as source into object-based and XML databases as targets. Database migration from the source into each of the targets is discussed in detail including semantic enrichment, schema translation and data conversion. Based on a detailed analysis of the existing literature, we conclude that an existing RDB can be migrated into object-based/XML databases according to available database standards. We propose an integrated method for migrating an RDB into object-based/XML databases using an intermediate Canonical Data Model (CDM), which enriches the source database's semantics and captures characteristics of the target databases. A prototype has been implemented, which successfully translates CDM into object-oriented (ODMG 3.0 ODL), object-relational (Oracle 10g) and XML schemas.",
isbn="978-3-540-85654-2"
}

\[McKemmish2001] 
@Article{McKemmish2001,
author="McKemmish, Sue",
title="Placing records continuum theory and practice",
journal="Archival Science",
year="2001",
month="Dec",
day="01",
volume="1",
number="4",
pages="333--359",
abstract="This article provides an overview of evolving Australian records continuum theory and the records continuum model, which is interpreted as both a metaphor and a new world-view, representing a paradigm shift in Kuhn's sense. It is based on a distillation of research findings drawn from discourse, literary warrant and historical analysis, as well as case studies, participant observation and reflection. The article traces the emergence in Australia in the 1990s of a community of practice which has taken continuum rather than life cycle based perspectives, and adopted postcustodial approaches to record keeping and archiving. It ``places'' the evolution of records continuum theory and practice in Australia in the context of a larger international discourse that was reconceptualizing traditional theory, and ``reinventing'' records and archives practice.",
issn="1573-7519",
doi="10.1007/BF02438901",
url="https://doi.org/10.1007/BF02438901"
}



CCSDS, 2012. Reference Model for an Open Archival Information
System (OAIS). Recommended Practice. Available at: <https://public.ccsds.org/Pubs/650x0m2.pdf>
\[Accessed 21 december 2018\].



Lim Siew Lin, Chennupati K. Ramaiah, Pitt Kuan Wal, (2003) "Problems in the preservation of electronic records", Library Review, Vol. 52 Issue: 3, pp.117-125, https://doi.org/10.1108/00242530310465924
