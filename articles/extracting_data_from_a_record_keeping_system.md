





You have some stuff in the DQ report

| Original file format | Preservation file format | count |
|-------|-----------|---------|
| BMP   | JPEG      | 100
| CSS   | N/A       | 1
| DOC   | PDF/A     | 327 000
| DOCM  | PDF/A     | 12
| DOCX  | PDF/A     | 1 360
| DWF   | PDF/A     | 1 
| DWG   | PDF/A     | 29 
| DXF   | PDF/A     | 3
| EXE   | N/A       | 5 
| GIF   | JPEG      | 1227
| PDF   | PDF/A     | 342000 
| HTML  | PDF/A     | 19000 
| JPEG  | JPEG      | 4100
| LWP   | PDF/A     | 1431
| MOV   | MPEG2     | 1     
| ODT   | PDF/A     | 25
| PPT   | PDF/A     | 280
| PPTS  | PDF/A     | 1
| PPTX  | PDF/A     | 30
| PNG   | JPEG      | 600
| RTF   | PDF/A     | 395
| TIF   | TIFF      | 493
| TIFF  | TIFF      | 2
| TXT   | RA-TXT    | 42461
| XLS   | PDF/A     | 958
| XLSM  | PDF/A     | 1
| XLSX  | PDF/A     | 110
| XPS   | PDF/A     | 1
| XML   | RA-XML    | 4 410

Table 1. A list of the count instances of various file formats across the 5 
databases and their preservation equivalent.



The problems with regards to data quality associated with migration and 
preservation are cost, volume, file format heterogeneity, time, technological 
obsolescence and difficulties with automation. 


Volume, time and cost are an interesting interrelated trio . 

* seen smaller databases with less problems. 
* exes should not have been registered. outlook.exe
* HTML files were assumed to be single entity



Time was a major factor as we did not have time to check what the exe was
Some excel files



 lack of referential integrity allows this to happe



hat is far more difficult to fix is where the table DOKTYPE
had duplicate primary key values on ND.DOKTYPE with the value “I”. The first was "I” for
incoming letter and the second was I for what we believe to be an incoming job application. 


