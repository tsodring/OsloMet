# Kort hva søknaden gjelder:
Tilegne kunnskap om hvordan teknologier som maskinlæring kan brukes til å måle avvikende kvalitet på avleverte uttrekk.

# Sammendrag av prosjektet:
Se på eksisterende uttrekk i lys av tidligere arbeid på feltet og utforske hvorvidt maskinlæring er en teknikk som kan brukes til å måle kvalitet eller manglende kvalitet på uttrekk.

# Omtale av prosjektet:
I et tidligere prosjekt støttet av ABMU (N4OK: Kvalitetsvurdering av avlevert materiale) målte vi kvaliteten på syv Noark 4 [uttrekk](http://edu.hioa.no/ark2100/current/syllabus/n4ok_rapport.pdf). Arbeidet ble fulgt opp av IKA Kongsberg og brukt til å avdekke potensiale store problemer med et innkommende uttrekk. Siden det har det vært lite fokus på verktøy for å måle kvaliteten på uttrekk. Det er blitt gjort mye arbeid på valideringsverktøy med prosjekter som Arkade, Documaster Noark 5 validator og flere, men disse har mer fokus på strukturelle problemer framfor problemer med innhold. Disse verktøyene ser kun på et enkelt uttrekk, mens vi ønsker å forstå uttrekk i større perspektiv.

I dette prosjektet ønsker vi å ta tak i følgende problemstillingen: Er det mulig å tilegne en helhets bilde om de kommunale elektroniske arkivene, på tvers av kommuner og bruke denne forståelsen til å måle kvaliteten av innholdet i uttrekkene på en bedre måte enn i dag.

Vi ønsker å bruke maskinlæring som utgangspunkt for å se hvorvidt teknologien egner seg til å danne en forståelse av arkivuttrekk som kan da brukes til å måle kvalitet. Resultatene av N4OK prosjektet er et godt utgangspunkt for å se nærmere på maskinlæring som datakvalitets verktøy.

En enkel måte å tenke på maskinlæring er at det er et mønstergenkjennings teknikk, at et program kan lære å reagere på eller gjenkjenne mønstre. Mønsteret vi ønsker å se finne er hvilken mønster har kommunale Noark uttrekk

Vi ønsker å se nærmere på hvilken elementer eksisterende Noark 4 uttrekk har som egner seg i en maskinlæring kontekst. Her er det kombinasjoner av metadata og dokumenter som kan brukes til å si noe om helheten til et uttrekk utifra innholdet i uttrekket. Med maskinlæring bør det være mulig å danne et bilde av hva normalen (i en maskinlæring kontekst) til kommunale uttrekk er. På sikt kunne andre uttrekk måles for å identifisere avvik fra normalen men også være med å danne grunnlaget for hva normalen er. Et slikt senere arbeid vil gi et veldig interessant oppsummering av de kommunale arkivene.

Maskinlæring er et veldig stort fagfelt, så noe av arbeidet her vil handle om å identifisere hvilken deler av maskinlæring som er relevant for å måle avvikende kvalitet i uttrekk.

Prosjektet skal levere en rapport med en arkitektur beskrivelse som forklarer hvorvidt maskinlæring er en teknikk som egner seg til å måle kvalitet på uttrekk og foreslå en konkret praktisk implementasjon.