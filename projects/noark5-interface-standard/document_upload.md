# Thoughts on handling documents

## 
 - About
 - Transaction on document uploads
 - Uploading a document to an empty documentObject
 - Uploading a document to partially complete documentObject
 - Converting to Archive format 

### About
The handling of documents requires a discussion on how they should be 

The interface standard has little description on how to handle missing 
information. There is a column in the interface standard called multiplicity
\[0..1\] that does give an indication whether or not a field is required. It
does not appear to detail if these fields are part of a POST request though.
A lot of fields when dealing with documentObjects appear to be optional, e.g.
checksum, checksumAlgorithm, format etc. The interface standard lacks a 
description of how a Noark core should process a file in the face og missing
data.

### Transaction on document uploads
The uploading of a document will normally occur at the same time that a
documentDescription and documentObject are created. There might perhaps
be a requirement to handle the creation of these two entitites and the
document upload as a single transaction with rollback.

Consider the following scenario. The user commits a document to a Noark core,
the documentDescription and documentObject are stored in the database, but the
persistence of the actual file to storage fails for some reason. If this is not
seen as a transaction the user is left with document details in the database 
but no actual document. Likewise, the database persistence could fail, but the 
document perssitence to disk succeeds. This particulary relevant in modern
microservices and cloud based computing environments. 

The interface does mentions a transaction, but that is in the scenario where a
client wishes to break a large file into smaller chunks.

It's important from a flexibility point of view to allow a NOark core to receive
documents / files without alle the fields set. This will make client development
easier and perhaps even increase document capture. Allowing the Noark core to
accept both filled in data and missing data is probably a good thing. The issue
really is how to deal with incorrect data.

### Uploading a document to an empty documentObject
If the documentObject is empty (except for systemId, createdData etc) then the
Noark core should extract all relevant data from the file and return a 
complete documentObject to the client with the data. After processings such a 
file the Noark core must 

 - Detect the mime-type of the object
 - Calculate a checksum
 - Calculate filesize
 - Deal with version and variant numbers

#### Detect the mime-type of the object
The mime-type, format (mime-type is a more formal definition of the file 
format) should be detected if not uploaded with the file. There needs to be a
discussion on whethere or not the mime-type as defined in the header should
automatically be used or the Noark core should detect the mime-type in all 
cases.  An example mime-type defined in a HTTP header is

   `Content-Type:application/vnd.oasis.opendocument.text`

In general, one should never trust the client with anything important.
It would be wise to require a file-detection process upon uploading. If the
detected file-type does not match the type set by the client the Noark core
should either reject the upload or silently accept it, but correct the 
filetype. The correct mime-type information will be returned in the 
documentObject returned to the client.

#### Calculate a checksum
The checksum algorithm defined in the Noark standard is SHA-256. If the 
checksum is not set, the Noark core should calculate the checksum and 
return it to the client in the documentObject. The client can then decide 
whether or not to verify that the checksum is correct.

#### Calculate filesize
I don't think there is a requirement to set the Content-length for an 
upload. If the field is missing, the Noark core must calculate the 
file length and return it as part of the documentObject. An example header
is seen here.

   `Content-Length:19610`

Again, it's probably not wise to use the value set by the client, but to let
the Noark core calculate the value itself. 

#### Deal with version and variant numbers
A documentDescrition can contain multiple versions and variants of a document 
and this is reflected in using the documentObject variant and version fields.
Versions only apply to archive versions, not production versions.
If these are not set by the client, then the Noark core must determine them
itself. This issue needs further discsussion

### Uploading a document with incorrect data
In the case where there is a wish for more strict use of documentObjects, the
interface standard could require that a Noark core checks the calculated metadata
after upload against the prevously set values in documentObject. The interface
standard should probably then require the rejection of the document upload as long
as the metadata and documentobject are not in sync. 

### Uploading a document to partially complete documentObject

It is probably wishful that a core allows for partial fields in the HTTP-header
and documentObject to be set and to allow the core fill in the remaining 
details. In such a case, any value that is set is checked and if the value is 
not correct the file upload is rejected.

#### Calculate a checksum
The checksum algorithm defined in the Noark standard is SHA-256. The use of
checksums here can be very important. It can be used to verify that the file 
that was uploaded was in fact uploaded correctly. The client could calculate
the checksum in advance of upload, set the correct value in a documentObject
and then upload the document. On seeing the value for checksum is correct,
the Noark core could then reject the upload if the calculated checksum is
not correct.

### Converting to Archive format 
I'm not sure if the method to convert to archive format is defined. The 
simplest approach would be to use something like:

   `https://localhost/noark5v4/api/arkivstruktur/dokumentobjekt/be79c5a6-dda2-42e1-b647-e76c3eb82df7/konverterFil`

and use the  HTTP-verb PUT. Although it's perhaps not clear if it should be a 
POST or PUT. A PUT is used to update something, while a POST is used to create
something. This is the conversion (an update) of an existing thing, but it does
result in the creation of a new documentObject entity. The result if this would
be a documentObject containing all the relevant data.

Another approach is to use PATCH and JSON Patch where perhaps we can specify
the filetype to convert to in the actual conversion process. This should 
probably be set to prevent conversions to anything that's not a Norwegian 
National Archive approved archive format.



