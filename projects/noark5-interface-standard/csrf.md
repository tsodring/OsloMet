# Thoughts on CSRF and Noark 5

## 
 - About
 - What is CSRF
 - CSRF protection in nikita
 - CSRF protection in the interface standard
 - Conclusion

### About
[nikita](https://gitlab.com/OsloMet-ABI/nikita-noark5-core) is an implementation of the Noark 5 standard. In particular it is an implementation of the [Noark 5v4 interface standard](https://github.com/arkivverket/noark5-tjenestegrensesnitt-standard). The current version of this interface standard does not say anything about [CSRF](https://en.wikipedia.org/wiki/Cross-site_request_forgery). CSRF is something I have come across while developing nikita and have been unsure as to whether or not we have a CSRF issue in nikita or if CSRF is something the interface standard should be concerned with. Recently I came across an interesting blog [post](https://blog.acolyer.org/2018/06/27/secure-coding-practices-in-java-challenges-and-vulnerabilities/) that discusses the issue. In nikita we made an assumption that we are not vulnerable to the CSRF issue. This is based on the fact that we interact via JSON and use HTTP verbs. The blog links to a spring.io resource that discusses [CSRF](https://docs.spring.io/spring-security/site/docs/current/reference/html/csrf.html). From what I can tell, CSRF is an issue nikita and that interface standard should be aware of!

### What is CSRF
CSRF is a malicious exploit where a users session in for example a browser is hijacked by a third party and the the exploit reuses the user authorisation to upload or change information on the server. As per the wikipedia article, there are two approaches to preventing CSRF vulnerabilities. The first is to use the _synchronizer token pattern_, while the second is to rely on a _same origin policy_ that can be implemented with a **Cookie-to-header** token.

### CSRF protection in nikita
The reason CSRF hasn't been an issue in the nikita project is that we disable the CSRF protection mechanism ``csrf().disable()``. Initially this was introduced because we were having a lot of trouble gets [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing) to work and perhaps we just ignored the issue of CSRF once we got things up and running. Nor has CSRF been in issue in the standard we are implementing so we have blissfully ignored the problem. But potential security holes cannot be an _intended_ feature of an application under development! 

### CSRF protection in the interface standard
Should CSRF protection be enabled by default as part of the standard? Surely that has to be a rhetorical question! If we know that there is a potential vulnerability, not preventing it means the security hole is an intended feature. Perhaps we need to see an actual example of the exploit working to ensure the people in charge of the standard understand the issue.

### Conclusion
Unfortunately there seems to be little discussion around how to move the interface standard forward so it's difficult to push this discussion. My opinion is that CSRF should be enabled by default in the interface standard. What's left for discussion is how it should be implemented.
