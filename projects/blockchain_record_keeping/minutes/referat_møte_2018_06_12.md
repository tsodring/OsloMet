
# Blokkjede prosjektmøte #1


   *Dato*: 2018-06-12

   *Sted*: Oslo Met 



## Agenda


 - 11.00 Velkommen og bli kjent

 - 11.30 Arbeidslunsj

 - 12.00 Gjennomgang av søknaden

 - 12.00 Utarbeide arbeidsplan


## Til stede


 - Tor Eivind Johansen (KDRS)

 - Svein Ølnes (Vestlandsforskning)

 - Jean-Philippe Andre Caquet (Trondheim kommune)

 - Thomas Sødring (OsloMet)


## Status


Vi kommer sent i gang med prosjektet. Det kan vise seg vanskelig å bli ferdig 

innen utgangen av 2018, og det er mulig at prosjektet strekker inn i første

kvartal 2019.


## Gjennomgang av søknaden


Vi gikk gjennom [søknaden](https://gitlab.com/tsodring/OsloMet/blob/master/projects/blockchain_record_keeping/blockchain_recordkeeping_2017.md) og diskuterte hva som skal leveres. 


     1. Hvordan blokkjede kan være grunnlag til neste generasjons eInnsyn

     2. Lettere kontroll og tilsynsarbeid 

     3. Hvordan kan blokkjede brukes som uttrekk

     4. Hvilken rolle kan blokkjede ha i arkivet


Disse punktene førte til en del diskusjon. I søknaden er det en beskrivelse om

egenskapene til Noark som gjør at det kan brukes i en blokkjede. 


Vi må være tydelig på hvordan vi bruker blokkjede teknologi og hva vi ønsker å 

oppnå. Av og til kan diskusjonen gi inntrykk at blokkjede er løsningen og vi 

leter etter en problemstilling. 


### eInnsyn


Det er ikke helt uproblematisk å publisere lenker til saksmapper i en blokkjede.

Et eksempel der struktur informasjon kan være utforende er i en 

barnevernsaksmappe der en part mister rett til innsyn. Det ble nevnt at 

vedkommende ikke skal ha muligheten til å se videre strukturelle endringer i

saken i en slik tilfelle.


Det kan være lurt å prøve å begrense seg til en case der det er 

uproblematisk at arkivstrukturen (systemId og arkivenhet beskrivelse) er 

publisert i en blokkjede.


### Tilsynsarbeid


Dette er kanskje mer en kvalitetsikrings tilnærming som kan også brukes som en

del av tilsynsarbeidet.


### Rollen i henhold til langtidslagring


KDRS ønsker at vi er tydelig på hvordan arkivet kan bruke blokkjede for å sikre 

autentisitet gjennom dokumentenes livsløp og hvordan autentisiteten kan 

langtidslagres.


### Andre momenter 


Det er ønskelig at vi også vi ser på vårt arbeid i lys av det som skjer ellers 

med blokkjede og arkiv. Flere prosjekter og presentasjoner ble gjennomgått. [Blokkjede 

og arkiv](http://www.dlmforum.eu/index.php/component/jdownloads/category/101-2nd-day?Itemid=-1)

var et eksempel, og [archangel](http://blockchain.surrey.ac.uk/projects/archangel.html) 

prosjektet i regi av Nasjonalarkivet i Storbritannia. Det ble også nevnt at vi bør se på

[DAT](https://datproject.org/) prosjektet.


Det er et ønske at vi skal ut og snakke med andre som jobber med lignende 

problemstillinger. Archangel prosjektet er kanskje det nærmeste, men vi 

se på det etter sommerferien.


Det ble ikke laget et formell arbeidsplan enda, vi tar en diskusjon etter 

sommeren per epost.


